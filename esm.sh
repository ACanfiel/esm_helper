#!/bin/bash

version="6.7"
build="current_version"
source_dir="${HOME}/esm"
destination_dir="/opt/esm"

database="esm"
username="root"
password="public"

ip="127.0.0.1"
port="1099"

default="n"

current_dir=$(pwd)

while getopts "v:b:s:d:i:p:m:u:py" FLAG
do
  case $FLAG in
    v)
      version=$OPTARG
      ;;
    b) 
      build=$OPTARG
      ;;
    s)
      source_dir=$OPTARG
      ;;
    d)
      destination_dir=$OPTARG
      ;;
    i)
      ip=$OPTARG
      ;;
    p)
      port=$OPTARG
      ;;
    m)
      database=$OPTARG
      ;;
    u)
      username=$OPTARG
      ;;
    p)
      echo "please enter your password: "
      read -s password
      ;;
    y)
      default="y"
      ;;
  esac
done

args=("${@:$OPTIND}")

function separator {
  echo "--------------------------------------------------"  
}


function install {
  folder="R.${version}.0.${build}"

  if [ -d $destination_dir ]; then
    echo "Installation failed: ${destination_dir} already exists."
    exit 1
  fi

  echo "Creating temporary file for silent install..."
  tmpfile=$(mktemp /tmp/esm_script.XXXXXX)
  echo "INSTALLER_UI=silent" >> $tmpfile
  echo "CHOSEN_INSTALL_SET=ESM" >> $tmpfile
  echo "USER_INSTALL_DIR=${destination_dir}" >> $tmpfile
  echo "WWP_BROWSER=firefox" >> $tmpfile
  echo "WWP_BROWSER_PATH=/usr/bin/" >> $tmpfile
  echo "WWP_CHECKSUM_USER_INPUT=0" >> $tmpfile
  echo "WWP_USE_MYSQL=1" >> $tmpfile
  echo "WWP_USE_ORACLE=0" >> $tmpfile
  echo "WWP_DB_HOST=localhost" >> $tmpfile
  echo "WWP_DB_PORT=3306" >> $tmpfile
  echo "WWP_DB_NAME=${database}" >> $tmpfile
  echo "WWP_DB_USERNAME=${username}" >> $tmpfile
  echo "WWP_DB_PASSWORD=${password}" >> $tmpfile
  echo "WWP_DB_PASSWORD2=${password}" >> $tmpfile
  echo "WWP_DB_DIR=/usr" >> $tmpfile
  echo "WWP_RUN_AS_SERVICE=0" >> $tmpfile
  echo "WWP_DB_SERVICE_NAME=" >> $tmpfile

  separator
  cat $tmpfile
  separator

  echo "Installing ESM..."
  separator

  if [ $build = "current_version" ]; then
    /mnt/esm/builds/${version}/current_version/linux_x86/ESMInstall64.bin -f ${tmpfile}
  else
    /mnt/esm/builds/${version}/${folder}/linux_x86/ESMInstall64.bin -f ${tmpfile}
  fi

  
  exit_code=$?
  (grep -Hn3 ERROR /opt/esm/Ethernet_Services_Manager_*)
  echo "exit code: ${exit_code}"

  rm $tmpfile
}

function remove {
  separator
  echo "Removing esm from: ${destination_dir}"
  rm -rf ${destination_dir}
}


function preAnt {
  separator

  echo "Changing directory to: ${source_dir}/Build"
  cd ${source_dir}/Build
  

  echo "Backing up build.properties..."
  cp build.properties build.properties.backup
  
  echo "Setting esm.home in build.properties to: ${destination_dir}"
  sed -r -i "s|esm\.home=.*|esm\.home=${destination_dir}|" build.properties
  chmod +x ant
}


function postAnt {
  echo "Removing modified build.properties file..."
  rm build.properties

  echo "Restoring original build.properties file..."
  mv build.properties.backup build.properties
}


function build {
  preAnt
  echo "Building esm using dev.std.all..."
  ./ant dev.std.all
  postAnt
}

function rebuild {
  preAnt
  echo "Rebuilding esm using dev.std..."
  ./ant dev.std.all
  postAnt
}


function db {
  separator
  echo "Connecting to database: ${username}@${database}"
  mysql --user=${username} --password=${password} --database=${database}
}

function dropdb {
  separator
  echo "Dropping database: ${database}"
  mysql --user=${username} --password=${password} -e "DROP DATABASE IF EXISTS ${database}"
}

# $1 template 
# $2 properties file
function nbi {
  separator
  if [ -f $2 ]; then 
    echo "NBI template $1 on ${ip}:${port} with attributes:"
    cat $2
    cd ${destination_dir}
    java -cp classes/ESMOSS.jar:classes/NmsServerClasses.jar com.esm.nbi.OSSProvisioningClient ${username} ${password} ${ip} ${port} $1 ${current_dir}/$2
  else
    echo "Invalid properties file: $2"
  fi
}


function start {
  separator
  echo "Starting ESM..."
  cd ${destination_dir}/bin
  ./startnms.sh
}

function startbg {
  separator
  echo "Starting ESM in background mode..."
  cd ${destination_dir}/bin
  (nohup ./startnms.sh & >/dev/null 2>&1)
}

function debug {
  separator
  echo "Starting ESM..."
  cd ${destination_dir}/bin
  ./debugnms.sh
}

function stop {
  separator
  echo "Stopping ESM..."
  cd ${destination_dir}/bin
  ./ShutDown.sh ${username} ${password}
}

function client {
  separator
  echo "Launching ESM Client..."
  cd ${destination_dir}/bin
  (nohup ./startApplicationClient.sh & >/dev/null 2>&1)
}

function clearlogs {
  separator
  echo "Clear old logs? [y/n]"
  if [ $(prompt) = "y" ]; then
    echo "Clearing all logs in: ${destination_dir}/logs"
    rm ${destination_dir}/logs/*
  fi
}

# $1 filename (optional)
function log {
  separator
  if [ $# -eq 0 ]; then 
    echo "Viewing all logs..."  
    less ${destination_dir}/logs/*
  elif [ -f ${destination_dir}/logs/$1 ]; then
    echo "Viewing log: $1"  
    less ${destination_dir}/logs/$1
  else
    echo "File not found: ${destination_dir}/logs/$1"
  fi
}


# $1 filename (optional)
function watchfile {
  separator
  if [ $# -eq 0 ]; then 
    echo "Watching log: stdout.txt"  
    watch -n 1 tail -n 25 /opt/esm/logs/stdout.txt
  elif [ -f ${destination_dir}/logs/$1 ]; then
    echo "Watching log: $1"  
    (watch -n 1 tail -n 25 ${destination_dir}/logs/$1)
  else
    echo "File not found: ${destination_dir}/logs/$1"
  fi
}

function prompt {
  if [ $default = "y" ]; then 
    response="y"
  else 
    read response
  fi
  
  echo $response  
}

# Configuration files 
function config {
  separator
  echo "Disable client inactivity timeout? [y/n]"
  if [ $(prompt) = "y" ]; then
    echo "Disabling..."
    sed -i 's|INACTIVITY_TIMEOUT_MIN="10"|INACTIVITY_TIMEOUT_MIN="0"|' ${destination_dir}/conf/clientparameters.conf
  fi

  separator
  echo "Populate seed.file with standard networks? [y/n]"
  if [ $(prompt) = "y" ]; then
    echo "Populating..."
    if grep -q 'NETWORK_ID=\"10.10.16.0\"' ${destination_dir}/conf/seed.file; then
      echo "Networks already exist."
    else    
      sed -i 's|</SEED>|<TO_DISCOVER>\n<net NETWORK_ID="10.10.16.0" NETMASK="255.255.255.0"/>\n<net NETWORK_ID="10.10.30.0" NETMASK="255.255.255.0"/>\n<net NETWORK_ID="10.10.132.0" NETMASK="255.255.255.0"/>\n</TO_DISCOVER>\n</SEED>|' ${destination_dir}/conf/seed.file
    fi

  fi

  separator
  echo "Set loggin levels to debugged default? [y/n]"
  if [ $(prompt) = "y" ]; then
    echo "Setting levels..."
     sed -i -r 's|LogLevel="[1|2|3|4]"|LogLevel="3"|' /opt/esm/conf/logging_parameters.conf
     sed -i -r 's|DisplayName="[CLIERR|CLIUSER]" LogLevel="[1|2|3|4]"|DisplayName="ESM" LogLevel="1"|' /opt/esm/conf/logging_parameters.conf
     sed -i -r 's|DisplayName="[ESM|PROV_LOG]" LogLevel="[1|2|3|4]"|DisplayName="ESM" LogLevel="4"|' /opt/esm/conf/logging_parameters.conf
  fi

  separator
  echo "Create debugnms.sh for Eclipse debugging? [y/n]"
  if [ $(prompt) = "y" ]; then
    echo "Creating debugnms.sh..."
    if [ -f ${destination_dir}/bin/debugnms.sh ]; then 
      echo "debugnms.sh already exists..."
    else 
      cp ${destination_dir}/bin/startnms.sh ${destination_dir}/bin/debugnms.sh
      sed -i 's|$JAVA_64_OR_32 -cp $CLASS_PATH|$JAVA_64_OR_32 -cp $CLASS_PATH  -Xdebug -Xrunjdwp:transport=dt_socket,server=y,address=8765|' ${destination_dir}/bin/debugnms.sh
    fi
  fi

  separator
  echo "Add IntelliJ debug hook to startnms.sh? [y/n]"
  if [ $(prompt) = "y" ]; then
    echo "Adding debug hook..."
    if grep -q "\-agentlib:jdwp=transport=dt_socket" ${destination_dir}/bin/startnms.sh; then
      echo "Hook already exists."
    else 
      sed -i 's|$JAVA_64_OR_32 -cp $CLASS_PATH -Dcatalina.home|$JAVA_64_OR_32 -cp $CLASS_PATH -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5005 -Dcatalina.home|' ${destination_dir}/bin/startnms.sh
    fi
  fi

  separator
  echo "Add this machine to the hosts file? [y/n]"
  if [ $(prompt) = "y" ]; then
    echo "Appending hosts file..."
    if grep -q "${HOSTNAME}" /etc/hosts; then 
      echo "An entry already exists for this hostname."
    else
      sudo bash -c "echo '${ip} ${HOSTNAME}' >> /etc/hosts"
    fi
  fi

}


separator
echo "version $version"
echo "build: $build"
echo "source_dir: $source_dir"
echo "destination_dir: $destination_dir"
echo "database: $database"
echo "username: $username"
echo "password: ********"
echo "hostname: $HOSTNAME"
echo "ip: $ip"


if [ $# -eq 0 ]; then
  separator
  echo "usage: esm [command] [filename] [-OPTIONS]"
  echo ""
  echo ""
  echo "commands:"
  echo "  build                             - initializes the ant builder with dev.std.all"
  echo "                                      *** NOTE: Requires esm to have already been installed"
  echo "                                        creates a custom build.properties based on options specified"
  echo "  client                            - launces the ESM Java client"
  echo "  config                            - rewrites config files with standard developer options:"
  echo "                                      *** NOTE: only run this command once ***"
  echo "                                        disable inactivity timeout, populats common networks"
  echo "                                        sets debug log levels, creates a blocking debug hook for eclipse"
  echo "                                        creates a non-blocking debug hook for IntelliJ, and"
  echo "                                        appends the hostname to the hosts file"
  echo "  db                                - opens the esm mysql database"
  echo "  debug                             - clears logs then starts the esm server in eclipe's debug"
  echo "                                      mode (intelliJ debug is run by default by the start command)"
  echo "                                      reference:"
  echo "                                      http://www.ibm.com/developerworks/library/os-eclipse-javadebug/"
  echo "  dropdb                            - drops the database"
  echo "  install                           - installs the current build from esm/builds/current"
  echo "  logs                              - displays all logs using less"
  echo "                                      (q to quit, :n for next, :p for previous)"
  echo "  log                               - displays stdout.txt using less"
  echo "  nbi [template] [properties.file]  - executes the provisioning templates through the nbi using the"
  echo "                                      key-value-pairs provided in the properties file"
  echo "  rebuild                           - initializes the ant builder with dev.std"
  echo "                                      *** NOTE: Requires esm to have already been installed"
  echo "                                      *** NOTE: Requires that build has already been run"
  echo "                                        creates a custom build.properties based on options specified"
  echo "  reinstall                         - removes the destination directory, drops the esm db, and then"
  echo "                                      reinstalls the the current build"
  echo "  remove                            - clears the database and removes the esm installation"
  echo "  start                             - clears logs then starts the esm server"
  echo "                                      intelliJ debugging is always enabled"
  echo "  startbg                           - clears logs then starts the esm server as a background process"
  echo "  stop                              - stops the esm server"
  echo "  watch                             - displays the tail of the stdout.txt log"
  echo "  watch [filename]                  - displays the tail of the specified log"
  echo ""
  echo ""
  echo "options:                                          DEFAULT"
  echo "  -v [version]      sets version number           [${version}]"
  echo "  -b [build]        sets build number             [${build}]"
  echo "  -s [path]         sets source path              [${source_dir}]"
  echo "  -d [path]         sets destination (esm home)   [${destination_dir}]"
  echo "  -i [ip address]   sets ip                       [${ip}]"
  echo "  -p [port]         sets port                     [${port}]"
  echo "  -m [database]     sets mysql database           [${database}]"
  echo "  -u [username]     sets username                 [${username}]"
  echo "  -p                prompts for password to use   [********]"
  echo "  -y                sets default response to y    [${default}]"
  echo ""
  echo "*** NOTE: This script assumes you have //waantap1b/Engineering/LENS/ESM/Builds mounted in /mnt/esm/builds"
  echo "REFERENCE: https://confluence.ciena.com/display/ESM/Mount+ESM+Builds+Directory"
  

elif [ ${#args[@]} -eq 1 ] && [ ${args[0]} = "build" ]; then
  build

elif [ ${#args[@]} -eq 1 ] && [ ${args[0]} = "client" ]; then
  client

elif [ ${#args[@]} -eq 1 ] && [ ${args[0]} = "config" ]; then
  config
 
elif [ ${#args[@]} -eq 1 ] && [ ${args[0]} = "db" ]; then
  db

elif [ ${#args[@]} -eq 1 ] && [ ${args[0]} = "debug" ]; then
  clearlogs
  debug

elif [ ${#args[@]} -eq 1 ] && [ ${args[0]} = "install" ]; then
  install
  config

elif [ ${#args[@]} -eq 1 ] && [ ${args[0]} = "logs" ]; then
  log

elif [ ${#args[@]} -eq 2 ] && [ ${args[0]} = "log" ]; then
  log ${args[1]}

elif [ ${#args[@]} -eq 3 ] && [ ${args[0]} = "nbi" ]; then
  nbi ${args[1]} ${args[2]}

elif [ ${#args[@]} -eq 1 ] && [ ${args[0]} = "rebuild" ]; then
  rebuild

elif [ ${#args[@]} -eq 1 ] && [ ${args[0]} = "reinstall" ]; then
  remove
  dropdb
  install
  config

elif [ ${#args[@]} -eq 1 ] && [ ${args[0]} = "remove" ]; then
  dropdb
  remove

elif [ ${#args[@]} -eq 1 ] && [ ${args[0]} = "start" ]; then
  clearlogs
  start

elif [ ${#args[@]} -eq 1 ] && [ ${args[0]} = "startbg" ]; then
  clearlogs
  startbg

elif [ ${#args[@]} -eq 1 ] && [ ${args[0]} = "stop" ]; then
  stop

elif [ ${#args[@]} -eq 1 ] && [ ${args[0]} = "watch" ]; then
  watchfile

elif [ ${#args[@]} -eq 2 ] && [ ${args[0]} = "watch" ]; then
  watchfile ${args[1]}

else
  separator
  echo "Invalid command, for a list of commands type: esm"

fi

cd $current_directory

separator
echo ""
echo ""